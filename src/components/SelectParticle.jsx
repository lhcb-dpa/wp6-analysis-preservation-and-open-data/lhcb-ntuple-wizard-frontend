/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import * as React from "react";
import PropTypes from "prop-types";
import {MathComponent} from "mathjax-react";
import Select from "react-select";

import MetadataContext from "../contexts/MetadataContext";

class SelectParticle extends React.Component {
    static contextType = MetadataContext;

    allowedParticles = () =>
        this.props.onlyHeads
            ? [...new Set(Object.values(this.context.metadata.decays).map((decay) => decay.descriptors.list[0]))]
            : this.props.onlyKnown
            ? Object.keys(this.context.metadata.particleProperties).filter(
                  (key) => !this.context.metadata.particleProperties[key].hide
              )
            : Object.keys(this.context.metadata.particleProperties);

    render() {
        let remainingProps = this.props;
        delete remainingProps.options;
        const loaded = this.context.loaded.particleProperties && (!this.props.onlyHeads || this.context.loaded.decays);
        const options = loaded
            ? this.allowedParticles().map((key) =>
                  Object({
                      value: key,
                      label: (
                          <MathComponent tex={this.context.metadata.particleProperties[key]["latex"]} display={false} />
                      ),
                  })
              )
            : [];
        return (
            <div className="react-select form-control p-0">
                <Select
                    options={options}
                    isLoading={!loaded}
                    filterOption={(candidate, input) => candidate.value.toLowerCase().startsWith(input.toLowerCase())}
                    {...remainingProps}
                />
            </div>
        );
    }
}

SelectParticle.propTypes = {
    options: PropTypes.arrayOf(PropTypes.object),
    onlyHeads: PropTypes.bool,
    onlyKnown: PropTypes.bool,
};
export default SelectParticle;
