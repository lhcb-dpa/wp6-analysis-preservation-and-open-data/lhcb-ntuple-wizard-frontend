/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Stack} from "react-bootstrap";
import YearBadge from "./YearBadge";
import PolarityBadge from "./PolarityBadge";
import StrippingBadge from "./StrippingBadge";
import EventTypeBadge from "./EventTypeBadge";
import BKPath from "../lib/BKPath";

class Dataset extends React.Component {
    path = new BKPath(this.props.path);
    render() {
        const version = this.path.getStrippingVersion();
        const eventType = this.path.getEventType();
        const year = this.path.getYear();
        const polarity = this.path.getPolarity();
        const filename = this.path.getFilename();
        const strippingInfo = <StrippingBadge version={version} />;
        const eventTypeInfo = <EventTypeBadge eventType={eventType} />;
        const yearInfo = <YearBadge year={year} />;
        const polarityInfo = <PolarityBadge polarity={polarity} />;
        return (
            <span>
                {filename}
                <br />
                <Stack direction="horizontal" style={{flexWrap: "wrap"}} gap={1}>
                    {eventTypeInfo} {yearInfo} {polarityInfo} {strippingInfo}
                </Stack>
            </span>
        );
    }
}

Dataset.propTypes = {
    path: PropTypes.string,
};
export default Dataset;
