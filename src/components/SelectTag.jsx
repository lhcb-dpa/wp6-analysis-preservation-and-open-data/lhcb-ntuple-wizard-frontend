/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import * as React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import {OverlayTrigger, Tooltip} from "react-bootstrap";

import MetadataContext from "../contexts/MetadataContext";

class SelectTag extends React.Component {
    static contextType = MetadataContext;

    static createOptions(tags, filter) {
        return Object.keys(tags)
            .filter((key) => filter(tags[key]))
            .map((key) =>
                Object({
                    value: key,
                    label: (
                        <OverlayTrigger placement="bottom" overlay={<Tooltip>{tags[key]["description"]}</Tooltip>}>
                            <span>{key}</span>
                        </OverlayTrigger>
                    ),
                })
            );
    }

    render() {
        let remainingProps = this.props;
        delete remainingProps.options;
        delete remainingProps.isLoading;
        const loaded = this.context.loaded.userHints;
        const options = SelectTag.createOptions(
            loaded ? this.context.metadata.userHints[this.props.type] : [],
            this.props.filter ? this.props.filter : (_tag) => true
        );
        return (
            <div className="react-select form-control p-0">
                <Select options={options} isLoading={!loaded} {...remainingProps} />
            </div>
        );
    }
}

SelectTag.propTypes = {
    options: PropTypes.array,
    isLoading: PropTypes.bool,
    type: PropTypes.string,
    filter: PropTypes.func,
};
export default SelectTag;
