/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import memoize from "lodash.memoize";
import PropTypes from "prop-types";
import React from "react";
import {
    Alert,
    Badge,
    Button,
    Dropdown,
    DropdownButton,
    InputGroup,
    OverlayTrigger,
    Spinner,
    Tooltip,
} from "react-bootstrap";
import {Link} from "react-router-dom";
import Select from "react-select";
import MetadataContext from "../contexts/MetadataContext";
import DecaysList from "./DecaysList";
import SelectParticle from "./SelectParticle";
import SelectTag from "./SelectTag";

class DescriptorsSearch extends React.Component {
    static contextType = MetadataContext;

    state = {
        selectedContains: [],
        selectedHead: null,
        selectedHeadTags: [],
        selectedDecays: this.props.rows.map((row) => row.decay),
        selectedTags: null,
        selectedLine: null,
        chargeConjugateContains: true,
        chargeConjugateHeads: true,
        containsMatchType: "all",
        headMatchType: "exactly",
        tagMatchType: "none",
        headTagMatchType: "all",
        onlySelected: false,
    };

    updateState = (key) => (contents) => {
        this.setState({[key]: contents});
    };

    toggleState = (key) => () => {
        this.setState({[key]: !this.state[key]});
    };

    callbackFunction = (childData) => {
        this.setState({selectedDecays: childData});
    };

    createRow = (decay, id) => {
        return Object({
            id: id,
            decay: decay,
            lines: [],
            paths: [], // Currently-selected paths
            pathOptions: [], // Keep a history of valid paths
            dtt: false,
        });
    };

    sendData = () => {
        const selectedDecays = this.state.selectedDecays;
        const selectedDecayDescriptors = selectedDecays.map((decay) => decay.descriptors.plain);
        // Loop through rows and remove any not-selected decays
        let rows = [...this.props.rows].filter((row) => selectedDecayDescriptors.includes(row.decay.descriptors.plain));
        const existingDecayDescriptors = rows.map((row) => row.decay.descriptors.plain);
        // Loop through selected decays and add any new rows
        const newDecays = selectedDecays.filter((decay) => !existingDecayDescriptors.includes(decay.descriptors.plain));
        newDecays.forEach((decay) => {
            const rowIDs = rows.map((row) => row.id);
            let newRowID = 0;
            while (rowIDs.includes(newRowID)) {
                newRowID += 1;
            }
            rows.push(this.createRow(decay, newRowID));
        });
        this.props.parentCallback(rows.map((row) => ({...row, dtt: {config: row.dtt}}))); // callback expects row.dtt to be a DTTConfig
    };

    static variableMatch = (type, conditions) => {
        if (type === "all") {
            return conditions.every((x) => x);
        } else if (type === "any") {
            return conditions.some((x) => x);
        } else if (type === "none") {
            return !conditions.some((x) => x);
        } else {
            console.error("Unrecognised match type: " + type);
            return true;
        }
    };

    allStrippingLines = memoize(() => {
        const allLines = [].concat(
            ...Object.values(this.context.metadata.stripping).map((stream) => Object.keys(stream))
        );
        const uniqueLines = new Set(allLines);
        return [...uniqueLines];
    });

    filterDecays = (selectedTags) => {
        // XXX: selectedTags is passed as an arg because it's the only one with a non-empty default value.
        //      the value depends on async-loaded metadata, and I don't know how to put it in the state.
        // TODO: can probably do some refactoring in here (cf variableMatch)
        let newDecays = Object.values(this.context.metadata.decays);
        if (this.state.selectedContains) {
            newDecays = newDecays.filter((decay) => {
                const conditions = this.state.selectedContains.map((element) => {
                    const particle = element.value;
                    const antiparticle = this.context.metadata.particleProperties[particle]["antiparticle"];
                    return (
                        decay.descriptors.plain.includes(particle) ||
                        (this.state.chargeConjugateContains && decay.descriptors.plain.includes(antiparticle))
                    );
                });
                return DescriptorsSearch.variableMatch(this.state.containsMatchType, conditions);
            });
        }
        if (this.state.headMatchType === "exactly") {
            if (this.state.selectedHead) {
                const particle = this.state.selectedHead.value;
                const antiparticle = this.context.metadata.particleProperties[particle]["antiparticle"];
                newDecays = newDecays.filter((decay) => {
                    const head = decay.descriptors.list[0];
                    return head === particle || (this.state.chargeConjugateHeads && head === antiparticle);
                });
            }
        } else if (this.state.headMatchType === "category") {
            if (this.state.selectedHeadTags) {
                newDecays = newDecays.filter((decay) => {
                    const conditions = this.state.selectedHeadTags.map((element) => {
                        const tag = element.value;
                        const head = this.context.metadata.particleProperties[decay.descriptors.list[0]];
                        return head.tags.includes(tag);
                    });
                    return DescriptorsSearch.variableMatch(this.state.headTagMatchType, conditions);
                });
            }
        }
        if (this.state.onlySelected) {
            newDecays = newDecays.filter((decay) =>
                this.state.selectedDecays
                    .map((selectedDecay) => selectedDecay.descriptors.plain)
                    .includes(decay.descriptors.plain)
            );
        }
        if (selectedTags) {
            newDecays = newDecays.filter((decay) => {
                const conditions = selectedTags.map((element) => {
                    const tag = element.value;
                    return decay.tags.includes(tag);
                });
                return DescriptorsSearch.variableMatch(this.state.tagMatchType, conditions);
            });
        }
        if (this.state.selectedLine) {
            const line = this.state.selectedLine.value;
            newDecays = newDecays.filter((decay) => {
                const decayStreamLines = Object.keys(decay.lines);
                const decayLines = new Set(decayStreamLines.map((streamLine) => streamLine.split("/")[1]));
                return decayLines.has(line);
            });
        }
        return newDecays;
    };

    static DropdownMatchTypes(props) {
        return (
            <>
                {props.options.map((option) => (
                    <Dropdown.Item
                        key={option}
                        active={props.parent.state[props.stateItem] === option}
                        onClick={() => props.parent.setState({[props.stateItem]: option})}
                    >
                        {option}
                    </Dropdown.Item>
                ))}
            </>
        );
    }

    static DropdownChargeConjugate(props) {
        return (
            <>
                <Dropdown.Item
                    active={props.parent.state[props.stateItem]}
                    onClick={() => props.parent.setState({[props.stateItem]: true})}
                >
                    Include antiparticles
                </Dropdown.Item>
                <Dropdown.Item
                    active={!props.parent.state[props.stateItem]}
                    onClick={() => props.parent.setState({[props.stateItem]: false})}
                >
                    Exclude antiparticles
                </Dropdown.Item>
            </>
        );
    }

    getDecayList = () => {
        const strippingSelectedTags =
            this.state.selectedTags !== null // If a value has been chosen by the user...
                ? this.state.selectedTags // ... then set according to the state.
                : this.context.loaded.userHints // Otherwise, if the metadata is loaded, set the default value...
                ? SelectTag.createOptions(this.context.metadata.userHints.decayTags, (tag) => tag.hide)
                : []; // ... or empty if not loaded yet
        const filteredDecays = this.context.loaded.decays ? this.filterDecays(strippingSelectedTags) : [];
        const strippingDecayList = (
            <>
                {this.context.loaded.decays ? (
                    <DecaysList
                        key={JSON.stringify(filteredDecays)}
                        decays={filteredDecays}
                        selectedDecays={this.state.selectedDecays}
                        parentCallback={this.callbackFunction}
                    />
                ) : (
                    <Alert variant="warning">
                        <Spinner size="sm" animation="border" /> Loading decays...
                    </Alert>
                )}
            </>
        );
        return strippingDecayList;
    };

    getExtraInputs = () => {
        const strippingSelectedTags =
            this.state.selectedTags !== null // If a value has been chosen by the user...
                ? this.state.selectedTags // ... then set according to the state.
                : this.context.loaded.userHints // Otherwise, if the metadata is loaded, set the default value...
                ? SelectTag.createOptions(this.context.metadata.userHints.decayTags, (tag) => tag.hide)
                : []; // ... or empty if not loaded yet
        const strippingExtraInput = (
            <>
                <InputGroup size="sm">
                    <DropdownButton title={"Tags (" + this.state.tagMatchType + " of):"} variant="outline-secondary">
                        <DescriptorsSearch.DropdownMatchTypes
                            parent={this}
                            stateItem="tagMatchType"
                            options={["any", "all", "none"]}
                        />
                    </DropdownButton>
                    <SelectTag
                        isMulti
                        type="decayTags"
                        placeholder="Decay tags"
                        value={strippingSelectedTags}
                        onChange={this.updateState("selectedTags")}
                        closeMenuOnSelect={false}
                    />
                    <div className="react-select form-control p-0">
                        <Select
                            placeholder="Stripping line"
                            isClearable
                            options={
                                this.context.loaded.stripping
                                    ? this.allStrippingLines().map((line) => Object({value: line, label: line}))
                                    : []
                            }
                            onChange={this.updateState("selectedLine")}
                            isLoading={!this.context.loaded.stripping}
                        />
                    </div>
                </InputGroup>
            </>
        );
        return strippingExtraInput;
    };

    render() {
        const nSelected = this.state.selectedDecays.length;
        const {basePath} = this.props;
        const decayList = this.getDecayList();
        const extraInputs = this.getExtraInputs();
        return (
            <>
                <h3>Decay search</h3>
                <InputGroup size="sm">
                    <DropdownButton
                        title={
                            "Head (" +
                            (this.state.headMatchType === "exactly"
                                ? this.state.headMatchType
                                : this.state.headTagMatchType + " of") +
                            "):"
                        }
                        variant="outline-secondary"
                    >
                        <DescriptorsSearch.DropdownMatchTypes
                            parent={this}
                            stateItem="headMatchType"
                            options={["exactly", "category"]}
                        />
                        <Dropdown.Divider />
                        {this.state.headMatchType === "exactly" ? (
                            <DescriptorsSearch.DropdownChargeConjugate parent={this} stateItem="chargeConjugateHeads" />
                        ) : (
                            <DescriptorsSearch.DropdownMatchTypes
                                parent={this}
                                stateItem="headTagMatchType"
                                options={["any", "all", "none"]}
                            />
                        )}
                    </DropdownButton>
                    {this.state.headMatchType === "exactly" ? (
                        <SelectParticle
                            isClearable
                            onlyHeads
                            placeholder="Decay head"
                            onChange={this.updateState("selectedHead")}
                        />
                    ) : (
                        <SelectTag
                            isMulti
                            type="particleTags"
                            placeholder="Decay head"
                            filter={(tag) => !tag.hide}
                            onChange={this.updateState("selectedHeadTags")}
                        />
                    )}
                    <DropdownButton
                        title={"Contains (" + this.state.containsMatchType + " of):"}
                        variant="outline-secondary"
                    >
                        <DescriptorsSearch.DropdownMatchTypes
                            parent={this}
                            stateItem="containsMatchType"
                            options={["any", "all", "none"]}
                        />
                        <Dropdown.Divider />
                        <DescriptorsSearch.DropdownChargeConjugate parent={this} stateItem="chargeConjugateContains" />
                    </DropdownButton>
                    <SelectParticle
                        isMulti
                        onlyKnown
                        placeholder="Particles in decay..."
                        onChange={this.updateState("selectedContains")}
                        closeMenuOnSelect={false}
                    />
                    <InputGroup.Text>Show only selected:</InputGroup.Text>
                    <InputGroup.Checkbox onChange={this.toggleState("onlySelected")} />
                </InputGroup>
                {extraInputs}
                {decayList}
                <Link to={basePath}>
                    <OverlayTrigger overlay={<Tooltip>Return to production configuration</Tooltip>}>
                        <Button variant="success" type="submit" onClick={this.sendData}>
                            {"Select "}
                            <Badge pill bg="secondary">
                                {nSelected}
                            </Badge>
                        </Button>
                    </OverlayTrigger>
                </Link>
            </>
        );
    }
}

DescriptorsSearch.propTypes = {
    rows: PropTypes.arrayOf(PropTypes.object),
    parentCallback: PropTypes.func,
    basePath: PropTypes.string.isRequired,
};
export default DescriptorsSearch;
