/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import * as React from "react";
import PropTypes from "prop-types";
import {MathComponent} from "mathjax-react";
import {OverlayTrigger, Popover} from "react-bootstrap";

class Decay extends React.Component {
    render() {
        return (
            <OverlayTrigger
                overlay={
                    <Popover>
                        <Popover.Header>LoKi descriptor</Popover.Header>
                        <Popover.Body>
                            <code>{this.props.decay.descriptors.plain}</code>
                        </Popover.Body>
                    </Popover>
                }
            >
                <span>
                    {/*Wrapping with <span> seems to be necessary for the Tooltip to appear.*/}
                    <MathComponent tex={this.props.decay.descriptors.latex} display={this.props.display} />
                </span>
            </OverlayTrigger>
        );
    }
}

Decay.propTypes = {
    decay: PropTypes.object,
    display: PropTypes.bool,
};

export default Decay;
