/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Badge} from "react-bootstrap";

class YearBadge extends React.Component {
    render() {
        return (
            <Badge pill bg="success">
                {this.props.year}
            </Badge>
        );
    }
}

YearBadge.propTypes = {
    year: PropTypes.string,
};
export default YearBadge;
