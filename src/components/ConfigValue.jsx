/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

import React from "react";
import PropTypes from "prop-types";
import {Form} from "react-bootstrap";

/*
 * bool      -> <input type="checkbox" />
 * int       -> <input type="number" step=1 />
 * uint      -> <input type="number" step=1 min=0 />
 * float     -> <input type="number" />
 * str       -> <input type="text" />
 * text      -> <textarea />
 */

class ConfigValue extends React.Component {
    handleUpdate = (event) => {
        const {value} = event.target;
        const sanitisedValue =
            this.props.type === "int"
                ? Math.trunc(value)
                : this.props.type === "uint"
                ? Math.trunc(Math.abs(value))
                : this.props.type === "text"
                ? value.split(/\r?\n/)
                : this.props.param === "ExtraName"
                ? value.replaceAll(/[^\w]/g, "")
                : value;
        this.props.callback(sanitisedValue);
    };
    handleCheck = () => {
        this.props.callback(!this.props.value);
    };
    render() {
        const inputType = {
            bool: "switch",
            float: "number",
            int: "number",
            uint: "number",
            str: "text",
            text: "textarea",
        }[this.props.type];
        if (this.props.type === "bool") {
            return <Form.Check onChange={this.handleCheck} type={inputType} checked={this.props.value} />;
        } else {
            let inputBoxProps = {
                onChange: this.handleUpdate,
                placeholder: inputType === "textarea" ? this.props.defaultValue.join("\n") : this.props.defaultValue,
                value: inputType === "textarea" ? this.props.value.join("\n") : this.props.value,
            };
            inputBoxProps[inputType === "textarea" ? "as" : "type"] = inputType;
            if (this.props.type.includes("int")) {
                inputBoxProps.step = 1;
                if (this.props.type === "uint") {
                    inputBoxProps.min = 0;
                }
            }
            return <Form.Control {...inputBoxProps} />;
        }
    }
}

ConfigValue.propTypes = {
    type: PropTypes.string,
    callback: PropTypes.func,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.string),
    ]),
    defaultValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.string),
    ]),
    param: PropTypes.string,
};
export default ConfigValue;
