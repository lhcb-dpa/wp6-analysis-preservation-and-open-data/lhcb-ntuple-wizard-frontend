/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import * as React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import TupleTool from "./TupleTool";

import MetadataContext from "../contexts/MetadataContext";

class SelectTool extends React.Component {
    static contextType = MetadataContext;

    render() {
        let remainingProps = this.props;
        delete remainingProps.options;
        delete remainingProps.isLoading;
        const loaded = this.context.loaded.tupleTools;
        const options = loaded
            ? this.props.filterToolTags.map((allowedTag) =>
                  Object({
                      label: allowedTag,
                      options: Object.keys(this.context.metadata.tupleTools.tupleTools)
                          .sort()
                          .map((tool) => Object({value: tool, label: <TupleTool tool={tool} />}))
                          .filter((tool) =>
                              this.context.metadata.tupleTools.tupleTools[tool.value].tags.includes(allowedTag)
                          ),
                  })
              )
            : [];
        return (
            <div className="react-select form-control p-0">
                <Select options={options} isLoading={!loaded} {...remainingProps} />
            </div>
        );
    }
}

SelectTool.propTypes = {
    options: PropTypes.array,
    isLoading: PropTypes.bool,
    filterToolTags: PropTypes.arrayOf(PropTypes.string),
};
export default SelectTool;
