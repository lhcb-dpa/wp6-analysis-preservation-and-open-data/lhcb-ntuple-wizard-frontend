/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import {env, pipeline} from "@xenova/transformers";

// This is so that we will download the models from huggingface.co
env.allowLocalModels = false;
env.useBrowserCache = false;
// store the embeddings in a dict
let query_embedding;
/**
 * @type {Object.<string, Array>}
 */
let embedding_dict = {};
/**
 * @type {pipeline}
 */
let embedder;

// these will hold the information to tie variable explanations to TupleTools
let dataKeys;
let paths;
/**
 * @type {Array}
 * */
let vars;
/**
 * This function calculates the embedding and stores it in a dict.
 * If the text is already embedded, it will return the embedding from the dict.
 * @param {string} text
 * @param {bool} embedNew
 * @returns embeddingVector
 */
async function embed(text, embedNew = true) {
    if (text in embedding_dict) {
        return embedding_dict[text];
    }
    const e0 = await embedder(text, {pooling: "mean", normalize: true});
    if (embedNew) {
        embedding_dict[text] = e0.data;
    }
    return e0.data;
}
/**
 * This function calculates the cosine similarity between two embeddings.
 * @param {Array} corpus_embedding
 * @param {Array} query_embedding
 * @returns cosine similarity
 * */
function calcCosSim(corpus_embedding, query_embedding) {
    let dotProduct = 0;
    let queryMag = 0;
    let embMag = 0;
    let loop_length = query_embedding.length;
    // because the embeddings might have different dimensions
    if (query_embedding.length > corpus_embedding.length) {
        loop_length = corpus_embedding.length;
    }
    for (let i = 0; i < loop_length; i++) {
        dotProduct += query_embedding[i] * corpus_embedding[i];
        queryMag += query_embedding[i] * query_embedding[i];
        embMag += corpus_embedding[i] * corpus_embedding[i];
    }
    const sim = dotProduct / (Math.sqrt(queryMag) * Math.sqrt(embMag));
    return sim;
}
/**This is a helper function to read the nested json file containing the documentation.
 * @returns [vars, paths, dataKeys]
 */

function readJsonFile(data) {
    // go through nested object
    vars = [];
    paths = [];
    const iterateObject = (obj, path = "") => {
        Object.keys(obj).forEach((key) => {
            const currentPath = path ? `${path}.${key}` : key;
            if (obj[key] instanceof Object) {
                iterateObject(obj[key], currentPath);
            } else {
                vars.push(obj[key]);
                paths.push(currentPath);
            }
        });
    };

    // go through data and put it into an array
    const dataArray = Object.values(data);
    dataKeys = Object.keys(data);
    iterateObject(dataArray);
    return [vars, paths, dataKeys];
}

// worker on message definition
self.onmessage = async (event) => {
    const message = event.data;
    switch (message.type) {
        case "init":
            // load the model and reset the embedding dict
            embedding_dict = {};
            embedder = await pipeline("feature-extraction", message.model, {
                progress_callback: (progress) => {
                    self.postMessage({type: "progress", progress: progress});
                },
            });
            break;
        case "corpus": {
            // embed the corpus or load the embeddings
            readJsonFile(message.kgdoc); // this is so we can later match variables to tupletools

            Object.keys(message.emb).forEach((key) => {
                let embarr = [];
                for (const fl of message.emb[key].split(",")) {
                    embarr.push(
                        fl
                            .replace("[", "")
                            .replace("]", "")
                            .replace("np.float32", "")
                            .replace("(", "")
                            .replace(")", "")
                            .replace(" ", "")
                    );
                }
                embedding_dict[key] = embarr;
            });
            self.postMessage({type: "corpus"});
            break;
        }
        case "similarity": {
            //calculate query embedding and then calculate similarity
            query_embedding = await embed(message.query, false);
            let sim_dict = {};
            Object.keys(embedding_dict).forEach((key) => {
                sim_dict[key] = calcCosSim(embedding_dict[key], query_embedding);
            });
            // sort by highest similarity (closest to 1)
            const sortedSimDict = Object.fromEntries(Object.entries(sim_dict).sort(([, a], [, b]) => b - a));
            const top5 = Object.fromEntries(Object.entries(sortedSimDict).slice(0, 20));
            let loki_result = [];
            let ttool_result = [];
            let result = {loki: loki_result, ttool: ttool_result};
            Object.keys(top5).forEach((key) => {
                if (key === "basic,charged: : ") {
                    return;
                } // This is a workaround until we implement a fix in Analysis (and corresponding DaVinci release)
                let description = key.substring(key.indexOf(":") + 2);
                let variable = key.substring(0, key.indexOf(":") - 1);
                let varpath = paths[vars.indexOf(description)];
                let tupletool = dataKeys[varpath.split(".")[0]];
                if (tupletool === "LoKi_functors") {
                    if (top5[key] > 0.86) {
                        if (loki_result.length < 5) {
                            loki_result.push([description, variable, tupletool]);
                        }
                    }
                } else {
                    if (top5[key] > 0.75) {
                        if (ttool_result.length < 5) {
                            ttool_result.push([description, variable, tupletool]);
                        }
                    }
                }
            });
            if (loki_result.length === 0) {
                loki_result.push(["No matches found", "No matches found", "No matches found"]);
            }
            if (ttool_result.length === 0) {
                ttool_result.push(["No matches found", "No matches found", "No matches found"]);
            }
            self.postMessage({type: "result", result: result});
        }
    }
};
