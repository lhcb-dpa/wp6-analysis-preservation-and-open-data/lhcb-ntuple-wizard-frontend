/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import "../style/DecaysList.css";
import * as React from "react";
import PropTypes from "prop-types";
import InfiniteScroll from "react-infinite-scroll-component";
import DecayItem from "./DecayItem";
import {Alert, Spinner, ListGroup} from "react-bootstrap";
import config from "../config";

class DecaysList extends React.Component {
    state = {
        items:
            this.props.decays.length > config.batch_size
                ? this.props.decays.slice(0, config.batch_size)
                : this.props.decays,
        index: this.props.decays.length > config.batch_size ? config.batch_size - 1 : this.props.decays.length,
        selectedDecays: this.props.selectedDecays,
    };

    fetchMoreData = () => {
        setTimeout(() => {
            this.setState({
                items: this.state.items.concat(
                    this.props.decays.slice(this.state.index + 1, this.state.index + 1 + config.batch_size)
                ),
                index:
                    this.state.index + config.batch_size < this.props.decays.length
                        ? this.state.index + config.batch_size
                        : this.props.decays.length,
            });
        }, 1000);
    };

    callbackFunction = (childData) => {
        if (childData.selected) {
            const index = this.state.selectedDecays.indexOf(childData.decay);
            if (index > -1) {
                this.state.selectedDecays.splice(index, 1);
            }
        } else {
            this.state.selectedDecays.push(childData.decay);
        }
        this.props.parentCallback(this.state.selectedDecays);
    };

    getDecayItem = () => {
        const strippingDecayItem = this.state.items.map((decay) => (
            <DecayItem
                key={decay.descriptors.plain}
                decay={decay}
                selected={this.state.selectedDecays.some(
                    (selected) => selected.descriptors.plain === decay.descriptors.plain
                )}
                parentCallback={this.callbackFunction}
            />
        ));
        return strippingDecayItem;
    };

    render() {
        const decayItem = this.getDecayItem();
        const nShown = this.state.items.length;
        const nTotal = this.props.decays.length;
        if (nShown === 0) {
            return <Alert variant="danger">No matching decays</Alert>;
        } else {
            return (
                <>
                    <ListGroup variant="flush" id="scrollableDiv" className="decaysListClass">
                        <InfiniteScroll
                            dataLength={nShown}
                            next={this.fetchMoreData}
                            hasMore={nShown < nTotal}
                            loader={
                                <Alert variant="warning">
                                    <Spinner size="sm" animation="border" /> Loading next{" "}
                                    {Math.min(config.batch_size, nTotal - nShown)} decays...
                                </Alert>
                            }
                            scrollableTarget="scrollableDiv"
                        >
                            {decayItem}
                        </InfiniteScroll>
                    </ListGroup>
                    <Alert variant="light">
                        Showing {nShown} of {nTotal} matching decays
                    </Alert>
                </>
            );
        }
    }
}

DecaysList.propTypes = {
    decays: PropTypes.arrayOf(PropTypes.object),
    selectedDecays: PropTypes.arrayOf(PropTypes.object),
    parentCallback: PropTypes.func,
};
export default DecaysList;
