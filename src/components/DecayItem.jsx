/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {ListGroup, InputGroup, OverlayTrigger, Popover, Badge} from "react-bootstrap";
import StrippingLine from "./StrippingLine";
import Decay from "./Decay";
import DecayTag from "./DecayTag";

class DecayItem extends React.Component {
    state = {
        selected: this.props.selected,
        decay: this.props.decay,
    };
    sendData = () => {
        this.props.parentCallback(this.state);
    };

    select = () => {
        this.setState({selected: this.state.selected ? false : true});
        this.sendData();
    };

    getExtraInfo = () => {
        const nLines = Object.keys(this.props.decay.lines).length;
        const nTags = this.props.decay.tags.length;
        const tagInfo = this.props.decay.tags.map((tag) => <DecayTag key={tag} tag={tag} />);
        const strippingInfo = (
            <>
                <OverlayTrigger
                    placement="right"
                    overlay={
                        <Popover>
                            <Popover.Header>Stripping lines</Popover.Header>
                            <Popover.Body>
                                {Object.keys(this.props.decay.lines).map((line) => {
                                    const [stream, name] = line.split("/");
                                    return (
                                        <StrippingLine
                                            key={line}
                                            line={name}
                                            stream={stream}
                                            versions={this.props.decay.lines[line]}
                                        />
                                    );
                                })}
                            </Popover.Body>
                        </Popover>
                    }
                >
                    <Badge pill bg={this.state.selected ? "primary" : "secondary"}>
                        {nLines} Stripping line{nLines === 1 ? "" : "s"}
                    </Badge>
                </OverlayTrigger>
                {nTags > 0 ? tagInfo : ""}
            </>
        );
        return strippingInfo;
    };

    render() {
        const extraInfo = this.getExtraInfo();
        return (
            <ListGroup.Item as="li" onClick={this.select}>
                <InputGroup>
                    <InputGroup.Checkbox checked={this.state.selected} readOnly />
                    <div
                        className={
                            "react-select form-control p-2 " +
                            (this.state.selected ? "list-group-item-primary" : "list-group-item-light")
                        }
                    >
                        <Decay decay={this.props.decay} display={false} />
                        <br />
                        {extraInfo}
                    </div>
                </InputGroup>
            </ListGroup.Item>
        );
    }
}

DecayItem.propTypes = {
    selected: PropTypes.bool,
    decay: PropTypes.object,
    parentCallback: PropTypes.func,
};
export default DecayItem;
