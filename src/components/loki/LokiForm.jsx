/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import LokiEditor from "./LokiEditor.jsx";
import PropTypes from "prop-types";
import {InputGroup, FormControl, Alert} from "react-bootstrap";
import Grid from "@mui/material/Grid";

class LokiForm extends React.Component {
    state = {
        key: this.props.key,
        value: this.props.value,
        err: false,
        errMsg: "",
    };

    keyCallback = (event) => {
        const key = event.target.value.replaceAll(/[^\w]/g, "");
        if (!this.props.validateKey(key)) {
            this.setState({err: true, errMsg: "Key must be unique and not empty!"});
        } else {
            this.setState({err: false, errMsg: ""});
        }
        this.setState({key: key});
        this.props.keyCallback(key);
    };

    valueCallback = (value) => {
        this.setState({value: value});
        this.props.valueCallback(value);
    };

    render() {
        const errorMessage = this.state.err ? <Alert variant="danger">{this.state.errMsg}</Alert> : <></>;
        const margin = {"margin-left": "7px"};
        return (
            <InputGroup>
                <Grid container spacing={1}>
                    <Grid className="d-flex" item form="maincomponent" xs>
                        <div>
                            {errorMessage}
                            <FormControl
                                placeholder="Varibale Name"
                                onChange={this.keyCallback}
                                value={this.state.key}
                            />
                        </div>
                        <div style={margin}>
                            <LokiEditor
                                callback={this.valueCallback}
                                value={this.state.value}
                                lokiVars={this.props.lokiVars}
                                oneLine={true}
                                height={"24px"}
                                width={"10dvw"}
                            />
                        </div>
                    </Grid>
                </Grid>
            </InputGroup>
        );
    }
}

LokiForm.defaultProps = {
    key: "",
    value: "",
    validateKey: () => {
        true;
    },
    lokiActive: true,
};

LokiForm.propTypes = {
    key: PropTypes.string,
    value: PropTypes.string,
    keyCallback: PropTypes.func,
    valueCallback: PropTypes.func,
    validateKey: PropTypes.func,
    lokiVars: PropTypes.object,
    lokiActive: PropTypes.bool,
};
export default LokiForm;
