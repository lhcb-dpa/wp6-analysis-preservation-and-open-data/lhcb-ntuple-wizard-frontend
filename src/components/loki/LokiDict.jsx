/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import LokiEditor from "./LokiEditor.jsx";
import Grid from "@mui/material/Grid";

import PropTypes from "prop-types";
import {Button, Card, ListGroup, Modal} from "react-bootstrap";

import {PlusLg} from "react-bootstrap-icons";
import LokiForm from "./LokiForm.jsx";

class LokiDict extends React.Component {
    state = {
        lines: this.props.value,
        error: "",
        add: false,
        currentKey: "",
        currentValue: "",
    };

    handleClose = () => {
        this.setState({add: false});
    };

    handleAdd = () => {
        var lines = this.state.lines;
        const key = this.state.currentKey;
        const value = this.state.currentValue;
        lines[key] = value;
        this.setState({lines: lines});
        this.setState({currentKey: ""});
        this.setState({currentValue: ""});
        this.props.callback(this.state.lines);
    };

    handleRemove = (key) => {
        var lines = this.state.lines;
        delete lines[key];
        this.setState({lines: lines});
        this.props.callback(this.state.lines);
    };

    setValueByKey = (key, value) => {
        var lines = this.state.lines;
        lines[key] = value;
        this.setState({lines: lines});
        this.props.callback(this.state.lines);
    };

    setAddLine = () => {
        this.setState({add: true});
    };

    changeKey = (value) => {
        this.setState({currentKey: value});
    };

    validKey = (value) => {
        if (value in this.state.lines) {
            return false;
        }
        return true;
    };

    changeValue = (value) => {
        this.setState({currentValue: value});
    };

    render() {
        return (
            <Card>
                <Card.Header className="d-flex justify-content-between align-items-start">
                    <Card.Title>Loki Lines</Card.Title>
                    <Button variant="success" size="sm" onClick={this.setAddLine}>
                        <PlusLg />
                    </Button>
                </Card.Header>
                <ListGroup variant="flush">
                    {Object.keys(this.state.lines).map((key) => {
                        return (
                            <ListGroup.Item key={key} className="d-flex justify-content-between align-items-start">
                                <Grid container spacing={1}>
                                    <Grid className="d-flex" item form="maincomponent" xs>
                                        <span>{key}: </span>
                                        <LokiEditor
                                            callback={(value) => {
                                                this.setValueByKey(key, value);
                                            }}
                                            value={this.state.lines[key]}
                                            lokiVars={this.props.lokiVars}
                                            oneLine={true}
                                            height={"24px"}
                                            width={"40dvw"}
                                        />
                                        <Button variant="danger" size="sm" onClick={() => this.handleRemove(key)}>
                                            Remove
                                        </Button>
                                    </Grid>
                                </Grid>
                            </ListGroup.Item>
                        );
                    })}
                </ListGroup>
                {this.state.add ? (
                    <Modal show onHide={this.handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Add Dict Item</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <LokiForm
                                key={""}
                                value={""}
                                keyCallback={this.changeKey}
                                valueCallback={this.changeValue}
                                lokiVars={this.props.lokiVars}
                                validateKey={this.validKey}
                            />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.handleClose}>
                                Close
                            </Button>
                            <Button
                                variant="primary"
                                onClick={() => {
                                    this.handleAdd();
                                    this.handleClose();
                                }}
                                disabled={
                                    this.state.currentKey === "" ||
                                    this.state.currentValue === "" ||
                                    !this.validKey(this.state.currentKey)
                                }
                            >
                                Save Changes
                            </Button>
                        </Modal.Footer>
                    </Modal>
                ) : (
                    ""
                )}
            </Card>
        );
    }
}

LokiDict.propTypes = {
    callback: PropTypes.func,
    defaulValue: PropTypes.string,
    value: PropTypes.object,
    lokiVars: PropTypes.object,
};
export default LokiDict;
