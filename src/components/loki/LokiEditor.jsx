/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import MonacoEditor from "react-monaco-editor";
// import { monaco } from 'react-monaco-editor';
import * as monaco from "monaco-editor";
import {loader} from "@monaco-editor/react";

loader.config({monaco});

import Spinner from "react-bootstrap/Spinner";
import PropTypes from "prop-types";

let LokiData = {};
function createDependencyProposals(range) {
    // returning a static list of proposals, not even looking at the prefix (filtering is done by the Monaco editor),
    // here you could do a server side lookup
    let listOfFunctors = [];
    Object.keys(LokiData).forEach((key, _index) => {
        listOfFunctors.push({
            label: key,
            kind: monaco.languages.CompletionItemKind.Function,
            documentation: LokiData[key].description,
            insertText: key,
            range: range,
        });
    });
    return listOfFunctors;
}
loader.init().then((monaco) => {
    monaco.languages.registerCompletionItemProvider("python", {
        provideCompletionItems: function (model, position) {
            var word = model.getWordUntilPosition(position);
            var range = {
                startLineNumber: position.lineNumber,
                endLineNumber: position.lineNumber,
                startColumn: word.startColumn,
                endColumn: word.endColumn,
            };
            return {
                suggestions: createDependencyProposals(range),
            };
        },
    });

    monaco.languages.registerHoverProvider("python", {
        provideHover: function (model, position) {
            let hoverObjects = [];
            let matches = Object.keys(LokiData).filter((name) => {
                return model.findMatches(name, false, false, true, " (:,)*").length !== 0;
            });
            if (matches.length > 0) {
                matches.forEach((match) =>
                    model.findMatches(match, false, false, true).forEach((range) =>
                        hoverObjects.push({
                            match: match,
                            position: position,
                            range: range,
                            contents: [
                                {
                                    supportHtml: true,
                                    value: LokiData[match].documentation,
                                },
                            ],
                        })
                    )
                );
                for (const element of hoverObjects) {
                    if (contains(element.range, position)) {
                        // alert(element.contents[0].value)
                        return element;
                    }
                }
            }
            return {
                range: new monaco.Range(1, 1, model.getLineCount(), model.getLineMaxColumn(model.getLineCount())),
                contents: [
                    {
                        supportHtml: true,
                        value: '<span style="color red;">No matches</span>',
                        isTrusted: true,
                    },
                ],
            };
        },
    });
});

function contains(range, position) {
    if (range.range.startLineNumber <= position.lineNumber && range.range.endLineNumber >= position.lineNumber) {
        if (range.range.startColumn <= position.column && range.range.endColumn >= position.column) {
            return true;
        }
    }
    return false;
}

class LokiEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: this.props.returnList ? this.props.value.join("\n") : this.props.value,
            loaded: true,
        };
    }
    editorDidMount(editor, _monaco) {
        editor.focus();
    }
    onChange(newValue, _e) {
        if (this.props.oneLine) {
            newValue = newValue.replace(/[\n\r]/g, "");
        }
        this.setState({code: newValue});
        if (this.props.returnList) {
            newValue = newValue.split(/\r?\n/);
        }
        this.props.callback(newValue);
    }
    render() {
        const code = this.state.code;
        const lokiVars = this.props.lokiVars.lokiVariables;
        Object.keys(lokiVars).forEach((key) => {
            LokiData[key] = lokiVars[key];
        });

        const options = {
            autoIndent: "full",
            contextmenu: false,
            fontFamily: "monospace",
            fontSize: 18,
            lineHeight: 24,
            hideCursorInOverviewRuler: true,
            matchBrackets: "always",
            lineNumbers: "off",
            lineDecorationsWidth: 0,
            lineNumbersMinChars: 0,
            glyphMargin: false,
            wordWrap: "on",
            minimap: {
                enabled: false,
            },
            scrollbar: {
                horizontal: "hidden",
                vertical: "hidden",
                horizontalSliderSize: 0,
                verticalSliderSize: 0,
                horizontalScrollbarSize: 0,
                verticalScrollbarSize: 0,
            },
            scrollBeyondLastLine: false,
            selectOnLineNumbers: true,
            roundedSelection: false,
            readOnly: false,
            cursorStyle: "line",
            automaticLayout: true,
            fixedOverflowWidgets: true,
            links: true,
        };

        return this.state.loaded ? (
            <MonacoEditor
                width={this.props.width}
                height={this.props.height}
                language="python"
                theme="vs-light"
                value={code}
                options={options}
                onChange={this.onChange.bind(this)}
                editorDidMount={this.editorDidMount.bind(this)}
            />
        ) : (
            <Spinner animation="border" role="status"></Spinner>
        );
    }
}

LokiEditor.defaultProps = {
    value: "",
    oneLine: false,
    width: "30vh",
    height: "10vh",
    returnList: false,
};

LokiEditor.propTypes = {
    callback: PropTypes.func,
    value: PropTypes.string,
    defaultValue: PropTypes.string,
    lokiVars: PropTypes.object,
    oneLine: PropTypes.bool,
    height: PropTypes.string,
    width: PropTypes.string,
    returnList: PropTypes.bool,
};

export default LokiEditor;
