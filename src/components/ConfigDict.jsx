/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
//import ConfigValue from "./ConfigValue";
import {Card, ButtonGroup, Button, FormControl, Alert} from "react-bootstrap";

/*
 * {str:str}  -> <ConfigDict /> multiple rows of <input type="text" /> <input type="text" />
 * {str:text} -> <ConfigDict /> multiple rows of <input type="text" /> <textarea />
 */

/*
 * Keys have to be unique and not empty...
 * this may cause problems if the callback is executed on every keystroke
 */

class ConfigDict extends React.Component {
    state = {
        value: JSON.stringify(this.props.value, null, 2), // Object.keys(this.props.value).map((key) => [key, this.props.value[key].valueOf()]),
        edit: false,
        error: "",
    };
    toggleEdit = () => {
        this.setState({edit: !this.state.edit});
    };
    handleSave = () => {
        try {
            const value = JSON.parse(this.state.value);
            this.props.callback(value);
            this.setState({error: ""});
            this.setState({edit: false});
        } catch (e) {
            this.setState({error: `${e}`});
        }
    };
    handleUpdate = (event) => {
        const {value} = event.target;
        this.setState({value: value});
    };
    render() {
        const errorMessage = this.state.error ? <Alert variant="danger">{this.state.error}</Alert> : <></>;
        const buttonLabel = this.state.edit ? "Cancel" : "Edit";
        const saveButton = this.state.edit ? (
            <Button variant="primary" onClick={this.handleSave}>
                Save
            </Button>
        ) : (
            ""
        );
        return (
            <Card>
                <Card.Body>
                    {errorMessage}
                    <FormControl
                        as="textarea"
                        value={this.state.value}
                        onChange={this.handleUpdate}
                        disabled={!this.state.edit}
                    />
                    <ButtonGroup>
                        {saveButton}
                        <Button variant="outline-secondary" onClick={this.toggleEdit}>
                            {buttonLabel}
                        </Button>
                    </ButtonGroup>
                </Card.Body>
            </Card>
        );
    }
}

ConfigDict.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.objectOf(PropTypes.string),
        PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)),
    ]),
    callback: PropTypes.func,
};
export default ConfigDict;
