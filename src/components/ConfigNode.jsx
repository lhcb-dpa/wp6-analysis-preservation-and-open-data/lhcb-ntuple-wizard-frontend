/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import TupleTool from "./TupleTool";
import SelectTool from "./SelectTool";
import ConfigValue from "./ConfigValue";
import ConfigList from "./ConfigList";
import ConfigDict from "./ConfigDict";
import DeleteButton from "./DeleteButton";
import LokiDict from "./loki/LokiDict";

import {
    Button,
    ButtonGroup,
    Card,
    InputGroup,
    FormControl,
    Form,
    ListGroup,
    Modal,
    Accordion,
    Spinner,
    Row,
    Col,
    OverlayTrigger,
    Tooltip,
} from "react-bootstrap";
import {QuestionCircle, PlusLg, PencilSquare} from "react-bootstrap-icons";

import MetadataContext from "../contexts/MetadataContext";

class ParticleConfig extends React.Component {
    static contextType = MetadataContext;
    state = {
        dtt: this.props.dtt,
        openAddTool: false,
        openConfigTool: false,
        expandedTool: {tool: "", open: false},
        selectedToolClass: "",
        selectedToolName: "",
    };

    handleOpen = (type) => () => {
        this.setState({
            [`open${type}Tool`]: true,
        });
    };

    handleClose = (type) => () => {
        this.setState({
            [`open${type}Tool`]: false,
        });
        this.clearSelectedTool();
        this.props.parentCallbackClose();
        this.props.parentCallbackUpdated();
    };

    clearSelectedTool = () => {
        this.setState({
            selectedToolClass: "",
            selectedToolName: "",
        });
    };

    handleRemoveTool = (tool) => () => {
        this.state.dtt.removeTool(this.props.particleID, tool);
        this.setState({
            dtt: this.state.dtt,
        });
        this.props.parentCallbackUpdated();
    };

    handleConfigTool = (tool) => () => {
        const splitArr = tool.split("/");
        const toolClass = splitArr[0];
        const toolName = splitArr.length > 1 ? splitArr[1] : "";
        this.setState({
            openConfigTool: true,
            selectedToolClass: toolClass,
            selectedToolName: toolName,
        });
    };

    handleUpdateTool = (tool, parameter) => (value) => {
        let parameters = this.state.dtt.getToolConfig(this.props.particleID, tool);
        const signalUpdated = parameters[parameter] !== value;
        parameters[parameter] = value;
        this.state.dtt.configureTool(this.props.particleID, tool, parameters);
        this.setState({
            dtt: this.state.dtt,
        });
        if (signalUpdated) {
            this.props.parentCallbackUpdated();
        }
    };

    setToolClass = (option) => {
        this.setState({selectedToolClass: option.value});
    };

    setToolName = ({target}) => {
        this.setState({selectedToolName: target.value.replaceAll(/[^\w]/g, "")});
    };

    handleAddTool = () => {
        const {selectedToolClass, selectedToolName} = this.state;

        if (selectedToolClass) {
            this.state.dtt.addTool(this.props.particleID, selectedToolClass, selectedToolName);
            this.setState({
                dtt: this.state.dtt,
                selectedToolClass: "",
                selectedToolName: "",
            });
            this.props.parentCallbackUpdated();
        }
    };

    getSelectedTool = () => {
        const {selectedToolClass, selectedToolName} = this.state;
        let selectedTool = selectedToolClass;
        if (selectedToolName) {
            selectedTool = `${selectedToolClass}/${selectedToolName}`;
        }
        return selectedTool;
    };

    toolExists = () => {
        const selectedTool = this.getSelectedTool();
        return this.state.dtt.listTools(this.props.particleID).includes(selectedTool);
    };

    getParam = (tool, param) => {
        const toolClass = tool.split("/")[0];
        const toolInterface = this.context.metadata.tupleTools.tupleTools[toolClass].interface;
        const paramInfo = toolInterface.filter((i) => i.name === param);
        if (paramInfo.length === 1) {
            return paramInfo[0];
        } else {
            console.warn("Cannot find " + param + " in:");
            console.warn(toolInterface);
            return {type: "str", description: "Error loading description"};
        }
    };

    displayDoxygen = () => {
        const html = {
            __html: this.context.metadata.tupleTools.tupleTools[this.state.selectedToolClass].documentation,
        };
        return <div dangerouslySetInnerHTML={html} />;
    };

    render() {
        const toolList = this.state.dtt.listTools(this.props.particleID);
        const nTools = toolList.length;
        const selectedTool = this.getSelectedTool();
        const toolDocs = selectedTool ? (
            <Accordion>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>Documentation for {this.state.selectedToolClass}</Accordion.Header>
                    <Accordion.Body>
                        {this.context.loaded.tupleTools ? this.displayDoxygen() : <Spinner animation="outline" />}
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        ) : (
            ""
        );
        const selectToolProps = this.state.selectedToolClass === "" ? {value: null} : {};
        return (
            <>
                <Card>
                    <Card.Header className="d-flex justify-content-between align-items-start">
                        <span>
                            {nTools} TupleTool{nTools === 1 ? "" : "s"}
                        </span>
                        <Button variant="success" size="sm" onClick={this.handleOpen("Add")}>
                            <PlusLg />
                        </Button>
                    </Card.Header>
                    <ListGroup variant="flush">
                        {toolList.map((toolName) => {
                            return (
                                <ListGroup.Item
                                    key={toolName}
                                    className="d-flex justify-content-between align-items-start"
                                    {...{variant: toolName === selectedTool ? "secondary" : ""}}
                                >
                                    <TupleTool tool={toolName} />
                                    <ButtonGroup size="sm">
                                        <Button variant="outline-secondary" onClick={this.handleConfigTool(toolName)}>
                                            <PencilSquare />
                                        </Button>
                                        <DeleteButton outline action={this.handleRemoveTool(toolName)} />
                                    </ButtonGroup>
                                </ListGroup.Item>
                            );
                        })}
                    </ListGroup>
                </Card>
                {this.state.openAddTool ? (
                    <Modal show onHide={this.handleClose("Add")}>
                        <Modal.Header closeButton>Add TupleTool</Modal.Header>
                        <Modal.Body>
                            <InputGroup hasValidation>
                                <SelectTool
                                    filterToolTags={this.props.filterToolTags}
                                    placeholder="TupleTool class"
                                    onChange={this.setToolClass}
                                    {...selectToolProps}
                                />
                                <FormControl
                                    placeholder="TupleTool name"
                                    onChange={this.setToolName}
                                    disabled={this.state.selectedToolClass === ""}
                                    value={this.state.selectedToolName}
                                    isInvalid={this.toolExists()}
                                />
                                <FormControl.Feedback type="invalid">
                                    A {this.state.selectedToolClass} with the name &quot;{this.state.selectedToolName}
                                    &quot; already exists
                                </FormControl.Feedback>
                            </InputGroup>
                            <Button
                                variant="success"
                                disabled={!this.state.selectedToolClass || this.toolExists()}
                                onClick={this.handleAddTool}
                            >
                                Add tool
                            </Button>
                            {toolDocs}
                        </Modal.Body>
                    </Modal>
                ) : (
                    ""
                )}
                {this.state.openConfigTool ? (
                    <Modal show size="xl" fullscreen="lg-down" onHide={this.handleClose("Config")}>
                        <Modal.Header closeButton>Configure {selectedTool}</Modal.Header>
                        <Modal.Body>
                            {selectedTool ? (
                                <Form>
                                    {Object.entries(
                                        this.state.dtt.getToolConfig(this.props.particleID, selectedTool)
                                    ).map(([param, value]) => {
                                        const paramObj = {...this.getParam(selectedTool, param)};
                                        const {description, type} = paramObj;
                                        const defaultValue = paramObj["default"]; // TODO: get rid of JS keywords from metadata
                                        const list_re = /\[(\w+)\]/;
                                        const dict_re = /\{(\w+):(\w+)\}/;
                                        const configProps = {
                                            value: value,
                                            defaultValue: defaultValue,
                                            callback: this.handleUpdateTool(selectedTool, param),
                                        };
                                        const inputBox =
                                            type.match(list_re) !== null ? (
                                                <ConfigList type={type.match(list_re)[1]} {...configProps} />
                                            ) : type.match(dict_re) !== null ? (
                                                selectedTool.includes("LoKi__Hybrid__") ? (
                                                    <LokiDict
                                                        lokiVars={this.context.metadata.lokiVariables}
                                                        {...configProps}
                                                    />
                                                ) : (
                                                    <ConfigDict
                                                        keyType={type.match(dict_re)[1]}
                                                        valType={type.match(dict_re)[2]}
                                                        {...configProps}
                                                    />
                                                )
                                            ) : (
                                                <ConfigValue type={type} param={param} {...configProps} />
                                            );
                                        return (
                                            <Form.Group key={param} as={Row} className="mb-2">
                                                <Form.Label column sm={3}>
                                                    <OverlayTrigger overlay={<Tooltip>{description}</Tooltip>}>
                                                        <QuestionCircle />
                                                    </OverlayTrigger>
                                                    <code> {param} </code>
                                                </Form.Label>
                                                <Form.Label column sm={2}>
                                                    <code> {type} </code>
                                                </Form.Label>
                                                <Col sm={7}>{inputBox}</Col>
                                            </Form.Group>
                                        );
                                    })}
                                </Form>
                            ) : (
                                "No tool selected"
                            )}
                            {toolDocs}
                        </Modal.Body>
                    </Modal>
                ) : (
                    ""
                )}
            </>
        );
    }
}

ParticleConfig.propTypes = {
    dtt: PropTypes.object,
    parentCallbackClose: PropTypes.func,
    parentCallbackUpdated: PropTypes.func,
    particleID: PropTypes.arrayOf(PropTypes.string),
    filterToolTags: PropTypes.arrayOf(PropTypes.string),
};
export default ParticleConfig;
