/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import {env} from "@xenova/transformers";

// To always download the model from huggingface.com
env.allowLocalModels = false;
env.useBrowserCache = false;
// Create a new worker
const worker = new Worker(new URL("./worker.jsx", import.meta.url), {
    type: "module",
});
/**
 * @type {function}
 * Promise resolve function for loading the model
 */
let loadResolve;
/**
 * @type {function}
 */
let queryResolve;

worker.onmessage = function (event) {
    const message = event.data;
    switch (message.type) {
        case "progress":
            if (message.progress.status === "ready") {
                loadResolve();
            }
            break;
        case "corpus":
            // the corpus is embedded
            queryResolve();
            break;
        case "result":
            queryResolve(message.result);
            break;
    }
};
/**
 *
 * @param {string} modelname
 * Load the model with the provided model name
 * @returns
 */
export async function loadModel(modelname) {
    worker.postMessage({type: "init", model: modelname});
    return new Promise((resolve) => {
        loadResolve = resolve;
    });
}
/**
 * Passes corpus embedding from backend to the worker
 * @returns
 */
export async function embedCorpus(metadata) {
    worker.postMessage({type: "corpus", kgdoc: metadata.metadata.kgdoc, emb: metadata.metadata.embedding});
    return new Promise((resolve) => {
        queryResolve = resolve;
    });
}
/**
 *
 * @param {string} query#
 * Calculate the similarity between the query and the corpus
 * @returns
 */
export async function getSimilarity(query) {
    worker.postMessage({type: "similarity", query: query});
    return new Promise((resolve) => {
        queryResolve = resolve;
    });
}
