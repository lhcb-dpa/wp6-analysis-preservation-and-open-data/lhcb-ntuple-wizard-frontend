/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Badge} from "react-bootstrap";

class PolarityBadge extends React.Component {
    render() {
        return (
            <Badge pill bg="danger">
                {this.props.polarity}
            </Badge>
        );
    }
}

PolarityBadge.propTypes = {
    polarity: PropTypes.arrayOf(PropTypes.string),
};
export default PolarityBadge;
