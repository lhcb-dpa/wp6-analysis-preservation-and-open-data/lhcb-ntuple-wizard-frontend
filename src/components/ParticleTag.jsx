/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Badge, OverlayTrigger, Tooltip, Spinner} from "react-bootstrap";
import MetadataContext from "../contexts/MetadataContext";
import config from "../config";

export class ParticleTag extends React.Component {
    static contextType = MetadataContext;
    render() {
        const background = this.context.loaded.userHints
            ? config.particleTagGroupStyles[this.context.metadata.userHints.particleTags[this.props.tag].group]
            : "dark";
        const foreground = "light";
        const description = this.context.loaded.userHints ? (
            this.context.metadata.userHints.particleTags[this.props.tag].description
        ) : (
            <Spinner animation="border" />
        );
        return (
            <OverlayTrigger overlay={<Tooltip>{description}</Tooltip>}>
                <Badge pill bg={background} text={foreground} {...this.props}>
                    {this.props.tag}
                </Badge>
            </OverlayTrigger>
        );
    }
}

ParticleTag.propTypes = {
    tag: PropTypes.string,
};
export default ParticleTag;
