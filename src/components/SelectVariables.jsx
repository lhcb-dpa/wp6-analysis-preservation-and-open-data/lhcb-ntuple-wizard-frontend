/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import PropTypes from "prop-types";
import React from "react";
import {Button, Col, Container, OverlayTrigger, Row, Spinner, Tooltip} from "react-bootstrap";
import {Route} from "react-router-dom";
import {MetadataContext, MetadataLoadWall} from "../contexts/MetadataContext";
import DecayTree from "./DecayTree";

class SelectVariables extends React.Component {
    constructor(props) {
        super(props);
        this.propsVar = this.props;
    }

    static contextType = MetadataContext;
    state = {
        selectedRows: this.props.rows.map((row) => ({...row, dtt: {config: row.dtt}})),
    };

    callbackFunction = (index) => (childConfig) => {
        let updatedRows = [...this.state.selectedRows];
        updatedRows.filter((row) => row.id === index)[0].dtt.config = childConfig;
        this.setState({selectedRows: updatedRows});
        this.props.parentCallback(updatedRows);
    };

    handeReturn = (history) => {
        history.push(this.propsVar.basePath);
    };

    render() {
        const innerContent = this.context.loaded.tupleTools ? (
            <>
                {this.state.selectedRows
                    .filter((row) => row.editTree)
                    .map((row) => {
                        return (
                            <Col lg="auto" key={row.id}>
                                <MetadataLoadWall>
                                    <DecayTree
                                        decay={row.decay}
                                        dttConfig={row.dtt.config}
                                        parentCallback={this.callbackFunction(row.id)}
                                    />
                                </MetadataLoadWall>
                            </Col>
                        );
                    })}
            </>
        ) : (
            <Col>
                <Spinner animation="border" role="status" size="sm" />
            </Col>
        );

        return (
            <>
                <h3>DecayTreeTuple configuration</h3>
                <Container>
                    <Row className="justify-content-lg-center">{innerContent}</Row>
                </Container>
                <Route
                    render={({history}) => (
                        <OverlayTrigger overlay={<Tooltip>Return to Stripping line and dataset selection</Tooltip>}>
                            <Button type="submit" onClick={() => this.handeReturn(history)}>
                                Return
                            </Button>
                        </OverlayTrigger>
                    )}
                />
            </>
        );
    }
}

SelectVariables.propTypes = {
    rows: PropTypes.arrayOf(PropTypes.object),
    parentCallback: PropTypes.func,
};
export default SelectVariables;
