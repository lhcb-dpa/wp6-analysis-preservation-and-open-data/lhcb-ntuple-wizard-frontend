/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {loadModel, embedCorpus, getSimilarity} from "./semantic";
import {useState, useMemo} from "react";
import MetadataContext from "../contexts/MetadataContext";
import {useContext} from "react";
import PropTypes from "prop-types";
import DeleteButton from "./DeleteButton";
function getmetafromcontext() {
    const context = useContext(MetadataContext);
    return context;
}

/**
 * Make the output of the search more readable
 * @param {Array} data
 * @returns
 */
function FormatOutput({data}) {
    let topchoice_ttool = [data.ttool[0]];
    let data_ttool = data.ttool.slice(1);
    let topchoice_loki = [data.loki[0]];
    let data_loki = data.loki.slice(1);
    return (
        <>
            <div>
                <h2>Best match (TupleTools):</h2>
                <ul>
                    {topchoice_ttool.map((item, index) => (
                        <li key={index}>
                            <strong>Matched TupleTool:</strong>{" "}
                            {item[2] === "LoKi_functors" ? "LoKi__Hybrid__TupleTool" : item[2]}
                            <ul>
                                <li>
                                    <strong>Variable name:</strong> {item[1]}
                                </li>
                                <li>
                                    <strong>Definition:</strong> {item[0]}
                                </li>
                            </ul>
                        </li>
                    ))}
                </ul>
                <h3>Other matches (TupleTools):</h3>
                <ul>
                    {data_ttool.map((item, index) => (
                        <li key={index}>
                            <strong>Matched TupleTool:</strong>{" "}
                            {item[2] === "LoKi_functors" ? "LoKi__Hybrid__TupleTool" : item[2]}
                            <ul>
                                <li>
                                    <strong>Variable name:</strong> {item[1]}
                                </li>
                                <li>
                                    <strong>Definition:</strong> {item[0]}
                                </li>
                            </ul>
                        </li>
                    ))}
                </ul>
            </div>
            <div>
                <h2>Best match (LoKi functors):</h2>
                <ul>
                    {topchoice_loki.map((item, index) => (
                        <li key={index}>
                            <strong>Matched TupleTool:</strong>{" "}
                            {item[2] === "LoKi_functors" ? "LoKi__Hybrid__TupleTool" : item[2]}
                            <ul>
                                <li>
                                    <strong>Variable name:</strong> {item[1]}
                                </li>
                                <li>
                                    <strong>Definition:</strong> {item[0]}
                                </li>
                            </ul>
                        </li>
                    ))}
                </ul>
                <h3>Other matches (LoKi functors):</h3>
                <ul>
                    {data_loki.map((item, index) => (
                        <li key={index}>
                            <strong>Matched TupleTool:</strong>{" "}
                            {item[2] === "LoKi_functors" ? "LoKi__Hybrid__TupleTool" : item[2]}
                            <ul>
                                <li>
                                    <strong>Variable name:</strong> {item[1]}
                                </li>
                                <li>
                                    <strong>Definition:</strong> {item[0]}
                                </li>
                            </ul>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    );
}

FormatOutput.propTypes = {
    data: PropTypes.objectOf(PropTypes.array).isRequired,
};

const SearchTTool = () => {
    const [inputData, setInputData] = useState({query: ""});
    const [responseData, setResponseData] = useState(null);
    let metadata = getmetafromcontext();
    // Load the model and embed the corpus when the component mounts
    useMemo(async () => {
        await loadModel("TaylorAI/gte-tiny");
        await embedCorpus(metadata);
    }, []);
    const handleChange = (event) => {
        setInputData({...inputData, query: event.target.value});
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        // clean input field
        setInputData({query: ""});
        let response = await getSimilarity(inputData.query);
        setResponseData(response);
    };
    const handleDelete = () => {
        setResponseData(null);
    };
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formQuery">
                <Form.Label>Search for TupleTools</Form.Label>
                <Form.Control type="text" value={inputData.query} onChange={handleChange} />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
            <DeleteButton disabled={!responseData} action={handleDelete} />
            {responseData && (
                <div>
                    <FormatOutput data={responseData} />
                </div>
            )}
        </Form>
    );
};
export default SearchTTool;
