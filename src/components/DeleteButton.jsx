/*****************************************************************************\
* (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Button, OverlayTrigger, Popover} from "react-bootstrap";
import {Trash} from "react-bootstrap-icons";

class DeleteButton extends React.Component {
    render() {
        if (this.props.disabled) {
            return null;
        }
        return (
            <OverlayTrigger
                trigger="click"
                placement="right"
                rootClose
                overlay={
                    <Popover>
                        <Popover.Body>
                            <Button variant="danger" onClick={this.props.action}>
                                Confirm
                            </Button>
                        </Popover.Body>
                    </Popover>
                }
            >
                <Button variant={this.props.outline ? "outline-danger" : "danger"} disabled={this.props.disabled}>
                    <Trash /> {this.props.children}
                </Button>
            </OverlayTrigger>
        );
    }
}

DeleteButton.propTypes = {
    action: PropTypes.func,
    outline: PropTypes.bool,
    disabled: PropTypes.bool,
    children: PropTypes.node,
};
export default DeleteButton;
