# LHCb Ntuple Wizard

LHCb Ntuple Wizard is an application to access large-scale open data from LHCb.

## Table of Contents

-   [Installation](#installation)
-   [Usage](#usage)
-   [Npm package](#npm-package)
-   [Contributing](#contributing)
-   [Questions](#questions)
-   [License](#license)

## Installation

Install Node/npm: https://nodejs.org/en

Run the following command to install the dependencies:

```bash
npm install
```

## Usage

#### Run the project using `npm`:

```bash
npm start
```

#### Run the project using `docker`:

```bash
docker buildx build -t ntuple-wizard .
docker run -itp 8080:8080 --rm ntuple-wizard
```

## Npm package

The Ntuple Wizard is also made available as a React component through an npm package ([see package here](https://www.npmjs.com/package/lhcb-ntuple-wizard)). To install it, run the following command:

```bash
npm install lhcb-ntuple-wizard
```

### Usage

```jsx
import React from "react";
import NtupleWizard from "lhcb-ntuple-wizard";

const App = () => {
    return (
        <NtupleWizard
            basePath="/"
            decaysPath="/decays"
            variablesPath="/variables"
            submitLocation="" // Leave empty to hide the submit button -> overrides hideDownloadButtons to false
            contactEmail="" // Leave empty to let the user fill in their email address
            hideDownloadButtons={true} // Can be overridden by submitLocation
        />
    );
};

export default App;
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Questions

If you have any questions about the repo, please [open an issue](https://gitlab.cern.ch/lhcb-dpa/wp6-analysis-preservation-and-open-data/lhcb-ntuple-wizard-frontend/-/issues/new).

## License

This project is licensed under The GNU General Public License V3.0. See [license](COPYING) for more information.
